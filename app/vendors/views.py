from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import status
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings
from app.accounts.models import User, Role
from django.db import IntegrityError
from datetime import datetime
from app.admins.serializers import ProductCategorySerializer, MarketingSaleSerializer, \
    AttachmentSerializer, SaleEnquirySerializer

from app.vendors.serializers import VendorSerializer, OfferSerializer, LocationSerializer

from app.vendors.models import Vendor, Offer, Location
from app.admins.models import Attachment, SaleEnquiry, MarketingSale
import math


@api_view(['POST'])
@permission_classes((AllowAny,))
def login(request):
    """
    Vendor login
    """
    if request.method == "POST":
        return access_login(request)


@api_view(['POST', 'PUT', 'GET'])
@permission_classes((IsAuthenticated,))
def offers(request):
    if request.method == "POST":
        return new_offer(request)
    if request.method == "PUT":
        return edit_offer(request)
    if request.method == "GET":
        return get_offer(request)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def offer_detail(request, offer_id):
    return get_offer_details(request, offer_id)


def create_token(user):
    '''
    create jwt token
    '''
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return token


def access_login(request):
    '''
    Check password and allow access to application
    Parameters:
            mobile, password
    '''
    mobile = request.data.get('mobile', None)
    password = request.data.get('password', None)

    token = None
    name = None
    is_first_login = False
    is_change_password_required = False
    last_login = None
    error = False
    if mobile and password:
        try:
            user_instance = User.objects.get(mobile=mobile)
            if user_instance.role.name == 1 \
                    and user_instance.is_active and user_instance.check_password(password):

                token = create_token(user_instance)
                name = user_instance.name
                is_first_login = user_instance.is_first_login
                is_change_password_required = user_instance.is_change_password_required
                last_login = user_instance.last_login_date
                user_instance.current_login_date = datetime.now()
                user_instance.save()
            else:
                error = True

        except User.DoesNotExist:
            error = True
    else:
        error = True

    if error:
        return Response({
            'code': 401,
            'message': "Unauthorised",
        }, status=status.HTTP_401_UNAUTHORIZED)

    else:
        return Response({
            'code': 200,
            'data': {
                'token': token,
                'name': name,
                'is_first_login': is_first_login,
                'is_change_password_required': is_change_password_required,
                'last_login': last_login
            }
        }, status=status.HTTP_200_OK)


def new_offer(request):
    '''
    Create new offer by vendor.
    Parameters: Title, description, from date, to date, discount, banner
    '''
    error = False
    error_message = False
    user = request.user

    # Check if user has access to the app
    has_access = check_access(user)
    print(has_access)
    if not has_access:
        return Response({
            'code': 401,
            'message': 'Unauthorised'
        }, status=status.HTTP_401_UNAUTHORIZED)

    title = request.data.get('title', None)
    description = request.data.get('description', None)
    from_date = request.data.get('from_date', None)
    to_date = request.data.get('to_date', None)
    discount = request.data.get('discount', None)
    banner = request.data.get('banner', None)
    vendor_error, vendor_instance = get_vendor(user)

    new = {
        "title": title,
        "description": description,
        "offer_type": 'D',
        "discount": discount,
        "banner": banner,
        "from_date": from_date,
        "to_date": to_date,
        "created_by": user.id,
        "vendor": vendor_instance.id
    }

    offer_serializer = OfferSerializer(data=new, partial=True)
    if offer_serializer.is_valid():
        offer_serializer.save()
    else:
        error = True
        error_message = offer_serializer.errors

    if not error:
        return Response({
            'code': 200,
            'message': 'Offer created and waiting for approval'
        }, status=status.HTTP_200_OK)

    else:
        return Response({
            'code': 400,
            'error': error_message,
            'message': "Error in creating offer",
        }, status=status.HTTP_400_BAD_REQUEST)


def edit_offer(request):
    '''
    Update Offer Information by Vendor
    Parameters: offer ID, Title, description, from date, to date, discount, banner,
    '''
    error = False
    error_message = False
    update_offer = {}
    user = request.user
    data = request.data
    # Check if user has access to the app
    has_access = check_access(user)
    print(has_access)
    if not has_access:
        print('45')
        return Response({
            'code': 401,
            'message': 'Unauthorised'
        }, status=status.HTTP_401_UNAUTHORIZED)

    # Check if Offer belongs to this user
    vendor_error, vendor_instance = get_vendor(user)

    offer_id = data.get('offer_id', None)
    if offer_id is not None:
        try:
            offer_instance = Offer.objects.get(id=offer_id)
            if offer_instance.vendor.id == vendor_instance.id:
                if data.get('title'):
                    update_offer['title'] = data.get('title')
                if data.get('description'):
                    update_offer['description'] = data.get('description')
                if data.get('from_date'):
                    update_offer['from_date'] = data.get('from_date')
                if data.get('to_date'):
                    update_offer['to_date'] = data.get('to_date')
                if data.get('discount'):
                    update_offer['discount'] = data.get('discount')
                if data.get('banner'):
                    update_offer['banner'] = data.get('banner')

                # changing back to inactive and pending for approval
                update_offer['is_active'] = False
                update_offer['approval_status'] = 'P'
                update_offer["updated_by"] = user.id

                # update Offer
                offer_serializer = OfferSerializer(
                    offer_instance, data=update_offer, partial=True)
                if offer_serializer.is_valid():
                    offer_serializer.save()
                else:
                    error = True
                    error_message = offer_serializer.errors
            else:
                error = True
                error_message = "Offer does not belongs to you"
        except Offer.DoesNotExist:
            error = True
            error_message = "Offer does not exists"

    else:
        error = True
        error_message = "Offer Id is required"

    if not error:
        return Response({
            'code': 200,
            'message': 'Offer updated and waiting for approval'
        }, status=status.HTTP_200_OK)

    else:
        return Response({
            'code': 400,
            'error': error_message,
            'message': "Error in updating offer",
        }, status=status.HTTP_400_BAD_REQUEST)


def get_offer(request):
    '''
    Get Offer list. 10 records per page by default.
    Filter options:
        1) Active or inactive
        2) Approved or Deneid
        3) active or inactive and upcoming month
        4)
    '''
    data = request.query_params
    filter_queries = {}
    if not data.get('page'):
        page = 1
    else:
        page = int(data.get('page'))

    if not data.get('per_page'):
        per_page = 10
    else:
        per_page = int(data.get('per_page'))

    user = request.user

    # Check if user has access to the app
    has_access = check_access(user)
    if not has_access:
        return Response({
            'code': 401,
            'message': 'Unauthorised'
        }, status=status.HTTP_401_UNAUTHORIZED)
    vendor_error, vendor_instance = get_vendor(user)

    old = (int(page) - 1) * per_page
    new = int(page) * per_page

    filter_queries['vendor'] = vendor_instance.id

    # filter by active status
    if data.get('is_active'):
        filter_queries['is_active'] = data.get('is_active')

    # filter by approval status
    if data.get('approval_status'):
        filter_queries['approval_status'] = data.get('approval_status')

    offer_list = Offer.objects.filter(
        **filter_queries).order_by('-updated_on')
    offer_serializer = OfferSerializer(
        offer_list[old:new], many=True)
    max_pages = math.ceil(len(offer_list)/per_page)

    return Response({
        'data': offer_serializer.data,
        'code': 200,
        "max_pages": max_pages
    }, status=status.HTTP_200_OK)


def get_offer_details(request, offer_id):
    '''
    Get Offer Details by Id
    '''
    error = False
    error_message = False

    if offer_id:
        try:
            offer_instance = Offer.objects.get(id=offer_id)
            offer_serializer = OfferSerializer(offer_instance)
            return Response({
                'data': offer_serializer.data,
                'code': 200,
            }, status=status.HTTP_200_OK)
        except Offer.DoesNotExist:
            error = True
            error_message = "Offer Not Found"

    return Response({
        'error': error_message,
        'code': 404,
    }, status=status.HTTP_404_NOT_FOUND)


def get_vendor(user):
    '''
    Get Vendor instance from session 
    '''
    try:
        vendor = Vendor.objects.get(user=user)
        return False, vendor
    except Vendor.DoesNotExist:
        return True, None


def check_access(user):
    '''
    Check role and access to the app
    '''
    role = user.role.name
    print(role, "ven")
    if role != 1:
        print('sunil')
        return False
    else:
        print('sunil1')

        return True
