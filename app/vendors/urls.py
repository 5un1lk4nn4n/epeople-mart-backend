from django.conf.urls import url
from django.urls import path
from rest_framework_jwt.views import verify_jwt_token
from app.vendors import views as vendorView

urlpatterns = [
    # API Url's

    path('auth/token-verify/', verify_jwt_token),
    path('auth/login', view=vendorView.login),
    path('offers/',
         view=vendorView.offers),
    path('offers/<int:offer_id>',
         view=vendorView.offer_detail),



]
