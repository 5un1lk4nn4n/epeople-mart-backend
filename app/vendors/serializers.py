from rest_framework import serializers
from django.utils import timezone

from app.vendors.models import Location, Offer, Vendor
from app.admins.serializers import ProductCategorySerializer, AttachmentSerializer


class LocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Location
        fields = '__all__'


class LocationSuggestionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Location
        fields = ('address_line_2', 'id')


class VendorDetailSerializer(serializers.ModelSerializer):
    location = LocationSerializer(read_only=True)
    category = ProductCategorySerializer(read_only=True)
    logo = AttachmentSerializer(read_only=True)
    aggrement = AttachmentSerializer(read_only=True)

    class Meta:
        model = Vendor
        fields = '__all__'


class VendorFreeSerializer(serializers.ModelSerializer):
    address = serializers.SerializerMethodField()
    category = serializers.SerializerMethodField()
    location = serializers.SerializerMethodField()
    company_name = serializers.SerializerMethodField()

    def get_address(self, obj):
        return obj.location.address_line_1+', '+obj.location.address_line_2+', '+obj.location.district+', '+obj.location.state+', '+obj.location.pin_code

    def get_category(self, obj):
        return {
            "name": obj.category.name,
            "icon": obj.category.icon
        }

    def get_company_name(self, obj):
        return obj.company_name.title()

    def get_location(self, obj):
        return {"latitude": obj.location.latitude, "longitude": obj.location.longitude}

    class Meta:
        model = Vendor
        fields = ('id','address','category','company_name','mobile','company_phone','website','email','views','location')


class VendorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vendor
        fields = '__all__'





class VendorListSerializer(serializers.ModelSerializer):
    category = serializers.SerializerMethodField()
    is_active = serializers.SerializerMethodField()
    is_registration_complete = serializers.SerializerMethodField()
    
    def get_is_active(self, obj):
        if obj.user:
            return obj.user.is_active 
        else :
            return False
    def get_is_registration_complete(self,obj):
        return True if obj.user else False

    def get_category(self, obj):
        return {
            "name": obj.category.name,
            "icon": obj.category.icon
        }

    class Meta:
        model = Vendor
        fields = ('id', 'company_name', 'owner_name',
                  'mobile', 'category', 'is_active', 'qr_code','is_registration_complete')


class OfferSerializer(serializers.ModelSerializer):
    vendor = VendorSerializer(read_only=True)

    class Meta:
        model = Offer
        fields = '__all__'


class OfferSearchSerializer(serializers.ModelSerializer):
    category = serializers.SerializerMethodField()
    vendor_shop_name = serializers.SerializerMethodField()
    banner = serializers.SerializerMethodField()
    vendor_id = serializers.SerializerMethodField()

    def get_category(self, obj):
        return obj.vendor.category.name

    def get_vendor_shop_name(self, obj):
        return obj.vendor.company_name.title()

    def get_banner(self, obj):
        return obj.banner.file.name

    def get_vendor_id(self, obj):
        return obj.vendor.id

    class Meta:
        model = Offer
        fields = ('id', 'vendor_shop_name', 'to_date',
                  'discount', 'banner', 'title', 'description', 'category', 'vendor_id')


class OfferCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Offer
        fields = '__all__'


class OfferUserSerializer(serializers.ModelSerializer):
    vendor_shop_name = serializers.SerializerMethodField()
    mobile = serializers.SerializerMethodField()
    company_phone = serializers.SerializerMethodField()
    website = serializers.SerializerMethodField()
    banner = serializers.SerializerMethodField()
    vendor_id = serializers.SerializerMethodField()
    address = serializers.SerializerMethodField()
    category = serializers.SerializerMethodField()
    location = serializers.SerializerMethodField()
    views = serializers.SerializerMethodField()
    

    def get_location(self, obj):
        return {"latitude": obj.vendor.location.latitude, "longitude": obj.vendor.location.longitude}
    def get_vendor_id(self, obj):
        return obj.vendor.id
    
    def get_views(self, obj):
        return obj.vendor.views

    def get_mobile(self, obj):
        return obj.vendor.mobile

    def get_company_phone(self, obj):
        return obj.vendor.company_phone

    def get_website(self, obj):
        return obj.vendor.website

    def get_vendor_shop_name(self, obj):
        return obj.vendor.company_name.title()

    def get_banner(self, obj):
        return obj.banner.file.name

    def get_address(self, obj):
        return obj.vendor.location.address_line_1+','+obj.vendor.location.address_line_2+',pin-'+obj.vendor.location.pin_code

    def get_category(self, obj):
        return obj.vendor.category.name

    class Meta:
        model = Offer
        fields = ('id', 'title', 'description', 'offer_type', 'discount',
                  'from_date', 'to_date', 'banner', 'vendor_id', 'vendor_shop_name', 'category', 'address','location','views','website','mobile','company_phone')


class OfferListSerializer(serializers.ModelSerializer):
    vendor_shop_name = serializers.SerializerMethodField()
    banner = serializers.SerializerMethodField()
    address = serializers.SerializerMethodField()
    category = serializers.SerializerMethodField()
    mobile = serializers.SerializerMethodField()
    website = serializers.SerializerMethodField()
    location = serializers.SerializerMethodField()
    views = serializers.SerializerMethodField()

    def get_address(self, obj):
        return obj.vendor.location.address_line_1+','+obj.vendor.location.address_line_2+',pin-'+obj.vendor.location.pin_code

    def get_vendor_shop_name(self, obj):
        return obj.vendor.company_name.title()

    def get_mobile(self, obj):
        return obj.vendor.mobile

    def get_website(self, obj):
        return obj.vendor.website

    def get_location(self, obj):
        return {"latitude": obj.vendor.location.latitude, "longitude": obj.vendor.location.longitude}

    def get_category(self, obj):
        return obj.vendor.category.name

    def get_banner(self, obj):
        return obj.banner.file.name

    def get_views(self, obj):
        return obj.vendor.views
    class Meta:
        model = Offer
        fields = ('id', 'title','vendor','description', 'offer_type', 'discount', 'from_date', 'to_date', 'is_active',
                  'approval_status', 'vendor_shop_name', 'banner', 'address', 'category', 'mobile', 'location','website','views')


class VendorLocationSerializer(serializers.ModelSerializer):
    category = serializers.SerializerMethodField()
    address = serializers.SerializerMethodField()
    mobile = serializers.SerializerMethodField()
    website = serializers.SerializerMethodField()
    shop_name = serializers.SerializerMethodField()


    def get_address(self, obj):
        return obj.location.address_line_1+','+obj.location.address_line_2+',pin-'+obj.location.pin_code

    def get_shop_name(self, obj):
        return obj.company_name.title()

    def get_mobile(self, obj):
        if obj.company_phone:
            return obj.company_phone+','+ obj.mobile
        else:
            return obj.mobile

    def get_website(self, obj):
        return obj.website
    
    def get_category(self, obj):
        return obj.category.name

    class Meta:
        model = Vendor
        fields = ('id','category','address','shop_name','mobile','website')


class VendorPlaceSerializer(serializers.ModelSerializer):
    place = serializers.SerializerMethodField()
 
    
    def get_place(self, obj):
        return obj.location.address_line_2


    class Meta:
        model = Vendor
        fields = ('id','place')
