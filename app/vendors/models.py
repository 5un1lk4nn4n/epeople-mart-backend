from django.db import models
from django.contrib import admin
from app.accounts.models import User
from app.admins.models import Attachment, ProductCategory
from app.fileupload.models import UserUpload


# from django.contrib.auth.models import PermissionsMixins
# from django.contrib.auth.base_user import AbstractBaseUser


class Location(models.Model):
    latitude = models.CharField(max_length=20, blank=True, null=True)
    longitude = models.CharField(max_length=20, blank=True, null=True)
    address_line_1 = models.CharField(max_length=150, blank=True, null=True)
    address_line_2 = models.CharField(max_length=150, blank=True, null=True)
    pin_code = models.CharField(max_length=10, blank=True, null=True)
    district = models.CharField(max_length=50, blank=True, null=True)
    state = models.CharField(max_length=50, blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('latitude', 'longitude')
        db_table = 'location'


class Vendor(models.Model):
    SMALL = 'SM'
    MEDIUM = 'MD'
    LARGE = 'LR'
    BUSINESS_TYPE = (
        (SMALL, 'Small'),
        (MEDIUM, 'Medium'),
        (LARGE, 'Large'),
    )
    company_name = models.CharField(max_length=150, blank=False, null=False)
    location = models.ForeignKey(
        Location, on_delete=models.SET_NULL, null=True, blank=False)
    user = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, null=True, blank=False, related_name="vebdor_user_id")
    owner_name = models.CharField(max_length=150, blank=False, null=False)
    mobile = models.CharField(
        max_length=15, blank=False, null=False)
    company_phone = models.CharField(
        max_length=15, blank=True, null=True)
    category = models.ForeignKey(
        "admins.ProductCategory", on_delete=models.SET_NULL, null=True, blank=False)
    website = models.CharField(max_length=255, blank=True, null=True)
    views = models.CharField(max_length=100, blank=True, null=True, default=0)
    email = models.CharField(
        max_length=150, blank=True, null=True)
    gst = models.CharField(max_length=20, blank=True, null=True)
    # = models.CharField(max_length=20, blank=True, null=True)
    business_type = models.CharField(
        max_length=15, choices=BUSINESS_TYPE, default=LARGE)
    logo = models.ForeignKey(
        "fileupload.UserUpload", on_delete=models.SET_NULL, null=True, blank=False, related_name="vendor_logo_attachment")
    discount_percentage = models.CharField(
        max_length=150, blank=False, null=False)
    has_coupon = models.BooleanField(default=False)
    coupon_value = models.CharField(max_length=50, blank=True, null=True)
    coupon_count = models.CharField(max_length=50, blank=True, null=True)
    qr_code = models.CharField(max_length=50, blank=False, null=False)
    hash_tag = models.CharField(max_length=255, blank=True, null=True)
    agreement = models.ForeignKey(
        "fileupload.UserUpload", on_delete=models.SET_NULL, null=True, blank=False, related_name="vendor_agreement_attachment")
    sales_by = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, null=True, blank=False, related_name="sales_user_id")
    sales_by_code = models.CharField(max_length=50, blank=True, null=True)
    coupon_description = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'vendor'


class Offer(models.Model):
    DISCOUNT = 'D'
    COUPON = 'C'
    OFFER_TYPE = (
        (DISCOUNT, 'Discount'),
        (COUPON, 'Coupon'),
    )

    PENDING = 'P'
    APPROVED = 'A'
    DENIED = 'D'
    APPROVAL_STAUS = (
        (PENDING, 'Pending'),
        (APPROVED, 'Approved'),
        (DENIED, 'Denied'),
    )
    vendor = models.ForeignKey(
        Vendor, on_delete=models.SET_NULL, null=True, blank=False)
    title = models.CharField(max_length=100, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    offer_type = models.CharField(
        max_length=8, choices=OFFER_TYPE, default=DISCOUNT)
    discount = models.DecimalField(max_digits=8, decimal_places=2)
    from_date = models.DateField()
    to_date = models.DateField()
    is_active = models.BooleanField(default=False)
    approval_status = models.CharField(
        max_length=8, choices=APPROVAL_STAUS, default=PENDING)

    comment = models.CharField(max_length=255, blank=True, null=True)
    tag = models.CharField(max_length=255, blank=True, null=True)
    banner = models.ForeignKey(
        UserUpload, on_delete=models.SET_NULL, null=True, blank=False)
    created_by = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, null=True, blank=False, related_name="offer_created_by")
    updated_by = models.ForeignKey(
        "accounts.User", on_delete=models.SET_NULL, null=True, blank=False, related_name="offer_updated_by")
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(int(self.discount))+'%'

    class Meta:
        db_table = 'offer'


admin.site.register(Location)
admin.site.register(Vendor)
admin.site.register(Offer)
