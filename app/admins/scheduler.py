import logging

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.executors.pool import ProcessPoolExecutor, ThreadPoolExecutor
from django_apscheduler.jobstores import register_events, register_job
from django.conf import settings
from app.vendors.model import Offer
from datetime import datetime


# Create scheduler to run in a thread inside the application process
scheduler = BackgroundScheduler(settings.SCHEDULER_CONFIG)


def offer_scheduler():
    '''
    Make offer active or inactive when validity data is acheieved
    '''

    print('calling Schedulers')
    today = datetime.today()

    # get Active offer but validity period completed
    Offer.objects.filter(
        is_active=True, approval_status="A", to_date__lt=today).update(is_active=False)

    # get inActive offer and approved offer but validity period starts
    Offer.objects.filter(
        is_active=False, approval_status="A", to_date__gte=today).update(is_active=True)


def start():
    print("Schediling ")
    if settings.DEBUG:
        # Hook into the apscheduler logger
        logging.basicConfig()
        logging.getLogger('apscheduler').setLevel(logging.DEBUG)

    # Adding this job here instead of to crons.
    # This will do the following:
    # - Add a scheduled job to the job store on application initialization
    # - The job will execute a model class method at midnight each day
    # - replace_existing in combination with the unique ID prevents duplicate copies of the job
    scheduler.add_job(offer_scheduler,
                      "cron", id="offer_scheduler", hour=11, replace_existing=True)

    # Add the scheduled jobs to the Django admin interface
    register_events(scheduler)

    scheduler.start()
