from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import status
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings
from app.accounts.models import User, Role
from django.db import IntegrityError
from datetime import datetime
from app.admins.serializers import ProductCategorySerializer, MarketingSaleSerializer, \
     SaleEnquirySerializer
from django.core.mail import send_mail
from api.settings import EMAIL_HOST_USER
from app.vendors.serializers import VendorSerializer, OfferSerializer, LocationSerializer, \
        VendorListSerializer, OfferListSerializer, VendorDetailSerializer, OfferCreateSerializer
from django.db.models import Q
from app.vendors.models import Vendor, Offer, Location
from app.fileupload.models import UserUpload
from app.admins.models import SaleEnquiry, MarketingSale, ProductCategory
from app.accounts.serializers import UserSerializer
import http.client
import re
import json
import math




@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def create_new_credintials(request):
    if request.method == "POST":
        return create_user(request)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def register_vendor(request):
    if request.method == "POST":
        step = request.data.get('step', None)
        if step == 1:
            return vendor_registration_step_1(request)
        elif step == 2:
            return vendor_registration_step_2(request)
        else:
            return Response({
                'code': 400,
                'error': ["Invalid step"],
                'message': "Error in registering vendor",
            }, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST', 'PUT', 'GET'])
@permission_classes((IsAuthenticated,))
def offers(request):
    if request.method == "POST":
        return new_offer(request)
    if request.method == "PUT":
        return edit_offer(request)
    if request.method == "GET":
        return get_offer(request)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def restore_vendor_registration(request):
    if request.method == "GET":
        return get_last_incomplete_vendor_registration(request)

@api_view(['GET','PUT'])
@permission_classes((IsAuthenticated,))
def employees(request):
    if request.method == 'GET':
        return get_employee_list(request)

    if request.method == 'PUT':
        return toggle_user_inactive(request)

@api_view(['GET','PUT'])
@permission_classes((IsAuthenticated,))
def vendors(request):
    if request.method == 'GET':
        return get_vendor_list(request)

    if request.method == 'PUT':
        return update_vendor(request)


@api_view(['GET','PUT'])
@permission_classes((IsAuthenticated,))
def vendor_information(request,vendor_id):
    if request.method == 'GET':
        return vendor_details(request,vendor_id)


        
@api_view(['GET','PUT'])
@permission_classes((IsAuthenticated,))
def categories(request):
    if request.method == 'GET':
        return get_categories_list(request)

def create_user(request):
    '''
    Create new user with roles - sales and admin. Send credentials to mobile using sms.
    Parameters:
            mobile, name, role
    '''
    mobile = request.data.get('mobile', None)
    role = request.data.get('role', None)
    name = request.data.get('name', None)
    error_message = None
    print(mobile, role, name)
    error = False

    # validate parameter and role
    is_ok = validate_new_user_data(mobile, name, role)

    if is_ok:
        try:
            role_instance = Role.objects.get(name=int(role))
            password = User.objects.make_random_password()

            payload = {
                "sender" : "EPMART",
                "route" : "4",
                "country":"91",
                "sms":[
                    {
                        "message":"Please use this credential: username:xxx"+mobile[5:]+" password:"+password,
                        "to":[mobile]
                    }
                ]
            }

            print(json.dumps(payload))
          
            user = User.objects.create_user(name=name,
                                            mobile=mobile,
                                            role=role_instance
                                            )
            user.set_password(password)
            user.save()
            send_sms(payload,"reg")
        except IntegrityError:
            error = True
            error_message = "User already exists"

    else:
        error = True
        error_message = "Missing or invalid parameters"

    if error:
        return Response({
            'code': 400,
            'message': error_message
        }, status=status.HTTP_400_BAD_REQUEST)

    else:
        return Response({
            'code': 200,
            'message': 'User created successfully'

        }, status=status.HTTP_200_OK)


def validate_new_user_data(mobile, name, role):
    '''
    Validate mobile, name, role [only sales-3 and admin-4]
    '''
    SALES_ROLE_ID = 3
    ADMIN_ROLE_ID = 4
    if not mobile and not name and not role:
        print("blank")
        return False

    try:
        int(mobile)
    except:
        print("mobile")
        return False

    try:
        int(role)
    except:
        print(role, "dd")
        return False

    if len(mobile) != 10:
        print("mobile number is  good")
        return False

    if role != SALES_ROLE_ID and role != ADMIN_ROLE_ID:
        print("role is not good")
        return False

    return True


def check_role(role, action, owner='super-admin'):
    '''
    Check  role access for action
    '''
    return True


def vendor_registration_step_1(request):
    '''
    Vendor registration in two steps.
    step 1:
        fill up required fields and generate QR code.
        Company name, company address[address line 1, address line 2, lattitude, longitude, city, state, Pin],
        mobile number, business category, vendor name, company phone number, website, email, gst,
        business type, discount percentage, discount coupons and step

    step 2:
        upload user registration document with finaly send credentials to mobile
    '''
    user = request.user
    error = False
    coupon_value = None
    coupon_count = None
    coupon_description = None

    company_name = request.data.get('company_name', None)
    if company_name is not None:
        company_name = company_name.title()
    location = request.data.get('location', None)
    owner_name = request.data.get('owner_name', None)
    mobile = request.data.get('mobile', None)
    company_phone = request.data.get('company_phone', None)
    category = request.data.get('category', None)
    website = request.data.get('website', None)
    email = request.data.get('email', None)
    gst = request.data.get('gst', None)
    business_type = request.data.get('business_type', None)
    discount_percentage = request.data.get('discount_percentage', None)
    has_coupon = request.data.get('has_coupon', False)
    hash_tag = request.data.get('hash_tag', None)
    if has_coupon:
        coupon_value = request.data.get('coupon_value', None)
        coupon_count = request.data.get('coupon_count', None)
        coupon_description = request.data.get('coupon_description',None)
    
    sales_by_code = request.data.get('sales_by_code', None)
    step = request.data.get('step', None)

    # Validate fields
    # Note Pending
    if step == 1:
            # generate QR Code
            qr_code = generate_qr_code(company_name)

            # save location gps data
            location_error, vendor_location_instance, error_message = save_vendor_location(
                location)
            if location_error:
                return Response({
                    'code': 400,
                    'error': error_message,
                    'message': "Error in registering vendor",
                }, status=status.HTTP_400_BAD_REQUEST)
            product_category_instance = ProductCategory.objects.get(id=category)
            # if no error in location, save vendor information
            print(product_category_instance.id,"fff",vendor_location_instance.id)
            new_vendor = {
                "company_name": company_name,
                "location": vendor_location_instance.id,
                "owner_name": owner_name,
                "mobile": mobile,
                "category": product_category_instance.id,
                "website": website,
                "email": email,
                "gst": gst,
                "business_type": business_type,
                "discount_percentage": discount_percentage,
                "has_coupon": has_coupon,
                "coupon_value": coupon_value,
                "coupon_count": coupon_count,
                "qr_code": qr_code,
                "company_phone": company_phone,
                "sales_by":user.id,
                "sales_by_code":sales_by_code,
                "coupon_description":coupon_description,
                "hash_tag":hash_tag

            }
            # save first step
            vendor_serializer = VendorSerializer(data=new_vendor, partial=True)
            if vendor_serializer.is_valid():
                new_vendor = vendor_serializer.save()
                print(request.user.id, "vendor id")
                new_sales = {
                    "user": request.user.id,
                    "vendor": new_vendor.id
                }
                sales_serializer = MarketingSaleSerializer(data=new_sales)
                if sales_serializer.is_valid():
                    sales_serializer.save()
                else:
                    print(sales_serializer.errors)
            else:
                error = True
                error_message = vendor_serializer.errors
     
            if not error:
                return Response({
                'code': 200,
                'data': {
                    "qr_code": qr_code,
                    "vendor_id": new_vendor.id
                },
                'message': 'Vendor saved successfully'

            }, status=status.HTTP_200_OK)

            else:
                return Response({
                'code': 400,
                'error': error_message,
                'message': "Error in Vendor Registration",
            }, status=status.HTTP_400_BAD_REQUEST)


def vendor_registration_step_2(request):
    '''
    Vendor registration in two steps.
    step 1:
        fill up required fields and generate QR code.
        Company name, company address[address line 1, address line 2, lattitude, longitude, city, state, Pin],
        mobile number, business category, vendor name, company phone number, website, email, gst,
        business type, discount percentage, discount coupons and step

    step 2:
        upload user registration document with finaly send credentials to mobile
    '''

    attachment_id = request.data.get('attachment_id', None)
    vendor_id = request.data.get('vendor_id', None)
    try:
        attachment_instance = UserUpload.objects.get(id=attachment_id)
        vendor_instance = Vendor.objects.get(id=vendor_id)
        vendor_user_instance = User.objects.get(mobile=vendor_instance.mobile)
    except User.DoesNotExist:
        role_instance = Role.objects.get(name=1)
        vendor_user_instance = User.objects.create_user(name=vendor_instance.owner_name,
                                                mobile=vendor_instance.mobile,
                                                role=role_instance
                                                )
        password = User.objects.make_random_password()
        # print(password,"vendor prin")

        vendor_user_instance.set_password(password)
        vendor_user_instance.save()
        send_email(vendor_instance.company_name,vendor_instance.mobile,password)

    update_vendor = {
            "is_active": True,
            "agreement": attachment_instance.id,
            "user" : vendor_user_instance.id
        }
    update_vendor_serializer = VendorSerializer(
                vendor_instance, update_vendor, partial=True)
    if update_vendor_serializer.is_valid():
        update_vendor_serializer.save()
    
        return Response({
                    'code': 200,
                    'message': 'Vendor registered successfully. Credintials has been sent to Vendor\'s mobile'

                }, status=status.HTTP_200_OK)
    else:
        return Response({
                    'code': 400,
                    'error': update_vendor_serializer.errors,
                    'message': "Error in Vendor Registration",
                }, status=status.HTTP_400_BAD_REQUEST)
 


def generate_qr_code(company_name):
    '''
    generate QR code in format EPM<first 3 char of company name><Joined date time>
    '''

    timestamp = datetime.now().strftime('%Y%m%d%H%M%S')
    code = "EPM"+company_name[:3].upper()+timestamp
    return code


def save_vendor_location(location):
    '''
    save location information of vendor
    '''
    
    new_location =  {
          "latitude":location['latitude'],
          "longitude":location['longitude'],
          "address_line_1": re.sub(' +',' ',location['address_line_1'].title()),
          "address_line_2": re.sub(' +', ' ', location['address_line_2'].title()),
          "pin_code": location['pin_code'],
          "district": re.sub(' +', ' ', location['district'].title()), 
          "state":re.sub(' +', ' ', location['state'].title())
        }
    vendor_location_serializer = LocationSerializer(data=new_location)
    if vendor_location_serializer.is_valid():
        new_vendor_location = vendor_location_serializer.save()
        return False, new_vendor_location, None
    else:
        return True, None, vendor_location_serializer.errors


def get_last_incomplete_vendor_registration(request):
    '''
    Get recent incomplete registration of vendor by sales team
    '''
    user = request.user
    vendor_info = None
    message = None
    last_vendor_registration = MarketingSale.objects.filter(
        user=user.id).order_by('-id')
    if len(last_vendor_registration) > 0:
        vendor_instance = Vendor.objects.get(
            id=last_vendor_registration[0].vendor.id)
        vendor_serializer = VendorSerializer(vendor_instance)

        if not vendor_serializer.data['is_active'] and vendor_serializer.data['id']:
            vendor_info = vendor_serializer.data
            message = "Vendor Information Restored"

    return Response({
        'code': 200,
        'data': vendor_info,
        'message': message

    }, status=status.HTTP_200_OK)


def new_offer(request):
    '''
    Create new offer by super admin instead of vendor. Need sms confirmation from vendor to proceed
    Parameters: Title, description, from date, to date, discount, banner, approval document
    '''
    error = False
    error_message = False
    user = request.user

    # Check if user has access to the app
    # has_access, response = check_access(user)
    # if not has_access:
    #     return response
    data = request.data
    title = data.get('title', None)
    description = data.get('description', None)
    from_date = data.get('from_date', None)
    to_date = data.get('to_date', None)
    discount = data.get('discount', None)
    banner = data.get('banner', None)
    vendor_id = data.get('vendor_id',None)
    tag = data.get('tag',None)
    if vendor_id:
        vendor_instance = Vendor.objects.get(id=vendor_id)
    else:
        vendor_instance = Vendor.objects.get(mobile=user.mobile)

    new = {
        "title": title,
        "description": description,
        "offer_type": 'D',
        "discount": discount,
        "banner": banner,
        "from_date": from_date,
        "to_date": to_date,
        "created_by": user.id,
        "vendor":vendor_instance.id,
        "tag":tag
    }

    offer_serializer = OfferCreateSerializer(data=new, partial=True)
    if offer_serializer.is_valid():
        offer_serializer.save()
    else:
        error = True
        error_message = offer_serializer.errors

    if not error:
        return Response({
            'code': 200,
            'message': 'Offer created and waiting for approval'
        }, status=status.HTTP_200_OK)

    else:
        return Response({
            'code': 400,
            'error': error_message,
            'message': "Error in creating offer",
        }, status=status.HTTP_400_BAD_REQUEST)


def edit_offer(request):
    '''
    Edit offer 1) change active/inactive
                2) change approval status approve/reject
                3) all fields in offer (only by super admin)
    '''
    user = request.user
    data = request.data
    update_type = data.get('update_type', None)
    offer_id = data.get('offer_id', None)
    error = False
    error_message = None

    try:
        offer_instance = Offer.objects.get(id=offer_id)
        # update type 1 - approve offer created by vendor or super admin
        if update_type == 1:
            approval_status = data.get('approval_status', None)
            comment = data.get('comment', None)
            update_offer = {
                "approval_status": approval_status,
                "comment": comment,
                "updated_by": user.id
            }

        # update type 2 - change offer to active or inactive
        if update_type == 2:
            if offer_instance.approval_status == "A":
                is_active = data.get('is_active', None)
                update_offer = {
                "is_active": is_active,
                "updated_by": user.id
                 }
            else:
                error =True
                error_message = "Offer is either not approved or is denied"

        # update type 3 - update full offer information on approval from vendor. only super admin
        if update_type == 3:
            pass
        if not error:
            # update in database
            update_offer_serializer = OfferSerializer(
                offer_instance, update_offer, partial=True)
            if update_offer_serializer.is_valid():
                update_offer_serializer.save()
            else:
                error = True
                error_message = update_offer_serializer.errors

    except Offer.DoesNotExist:
        error = True
        error_message = "Offer does not exist"

    if not error:
        return Response({
            'code': 200,
            'message': 'Offer updated'
        }, status=status.HTTP_200_OK)

    else:
        return Response({
            'code': 400,
            'error': error_message,
            'message': error_message,
        }, status=status.HTTP_400_BAD_REQUEST)




def update_vendor(request):
    '''
    Edit offer 1) change active/inactive
               2) all fields in offer (only by super admin)
    '''
    user = request.user
    data = request.data
    update_type = data.get('update_type', None)
    vendor_id = data.get('vendor_id', None)
    error = False
    error_message = None

    try:
        vendor_instance = Vendor.objects.get(id=vendor_id)
        # update type 1 - change offer to active or inactive
        if update_type == 1:
                vendor_user_instance = User.objects.get(mobile=vendor_instance.mobile)
                vendor_user_instance.is_active = not vendor_user_instance.is_active 
                vendor_user_instance.save()
              

        # update type 3 - update full offer information on approval from vendor. only super admin
        if update_type == 3:
            update_vendor_values = data.get('new_values')
            if not error:
                # update in database
                update_vendor_serializer = VendorSerializer(vendor_instance,
                update_vendor_values, partial=True)
                if update_vendor_serializer.is_valid():
                    update_vendor_serializer.save()
                else:
                    error = True
                    error_message = update_vendor_serializer.errors

        if update_type == 4:
            update_vendor_values = data.get('new_values')
            #update location
            new_location = update_vendor_values.get('location')
            update_location =  {
                "latitude":new_location['latitude'],
                "longitude":new_location['longitude'],
                "address_line_1": re.sub(' +',' ',new_location['address_line_1'].title()),
                "address_line_2": re.sub(' +', ' ', new_location['address_line_2'].title()),
                "pin_code": new_location['pin_code'],
                "district": re.sub(' +', ' ', new_location['district'].title()), 
                "state":re.sub(' +', ' ', new_location['state'].title())
            }
            
            if not error:
                location_instance = Location.objects.get(id=new_location.get('id'))
                update_location_serializer = LocationSerializer(location_instance,update_location,partial=True)
                if update_location_serializer.is_valid():
                    update_location_serializer.save()
                # update in database
                del update_vendor_values['location']
                update_vendor_serializer = VendorSerializer(vendor_instance,
                update_vendor_values, partial=True)
                if update_vendor_serializer.is_valid():
                    update_vendor_serializer.save()
                else:
                    error = True
                    error_message = update_vendor_serializer.errors
                
                # TODO Check if mobile number is edited and delete previus user mobile record 
                vendor_user_instance = User.objects.filter(mobile=vendor_instance.mobile)
                print(len(vendor_user_instance),"GI")
                if len(vendor_user_instance) > 0:
                    vendor_instance.user = vendor_user_instance[0]
                    vendor_instance.save()
                else:
                    # vendor Role
                    role_instance = Role.objects.get(name=1)
                    user = User.objects.create_user(name=vendor_instance.owner_name,
                                            mobile=vendor_instance.mobile,
                                            role=role_instance
                                            )
                    password = User.objects.make_random_password()

                    user.set_password(password)
                    user.save()
                    vendor_instance.user = user
                    vendor_instance.save()

    except (Vendor.DoesNotExist,User.DoesNotExist):
        error = True
        error_message = "Vendor does not exist"

    if not error:
        return Response({
            'code': 200,
            'message': 'Vendor updated'
        }, status=status.HTTP_200_OK)

    else:
        return Response({
            'code': 400,
            'error': error_message,
            'message': error_message,
        }, status=status.HTTP_400_BAD_REQUEST)


def get_offer(request):
    '''
    Get Offer list. 10 records per page by default. 
    Filter options: 
        1) Active or inactive
        2) Approved or Deneid
        3) active or inactive and upcoming month
        4) 
    '''
    data = request.query_params
    filter_queries = {}
    if not data.get('page'):
        page = 1
    else:
        page = int(data.get('page'))

    if not data.get('per_page'):
        per_page = 10
    else:
        per_page = int(data.get('per_page'))

    user = request.user
    role = user.role.name
    print(user.role.name)

    # NOTE: PENDING - NEED TO WORK ON ROLE PERMISSION !IMPORTANT
    # Check if user has access to the app
    # has_access, response = check_access(user)
    # if not has_access:
    #     return response

    old = (int(page) - 1) * per_page
    new = int(page) * per_page

    # filter_queries['vendor'] = user.id

    is_active = data.get('is_active')
    approval_status = data.get('approval_status')
    search_query = data.get('search')
    if role != 1:
        if is_active or approval_status or search_query:
            if search_query:
                offer_list = Offer.objects.filter(
                Q(vendor__company_name__icontains = search_query) | Q(tag__icontains = search_query) |Q(title__icontains = search_query) ).order_by('-created_on')
            else:
                # filter by active status
                if is_active:
                    filter_queries['is_active'] = is_active

                # filter by approval status
                if approval_status:
                    filter_queries['approval_status'] = approval_status

                offer_list = Offer.objects.filter(
                                        **filter_queries).order_by('-updated_on')
        else:
            offer_list = Offer.objects.all()
    else:
        vendor_instance = Vendor.objects.get(mobile=user.mobile)
        if is_active or approval_status or search_query:
            if search_query:
                offer_list = Offer.objects.filter(
                  Q(tag__icontains = search_query) |Q(title__icontains = search_query) , Q(vendor__user=user) ).order_by('-created_on')
            else:
                # filter by active status
                if is_active:
                    filter_queries['is_active'] = is_active
                    filter_queries['vendor__user'] = user

                # filter by approval status
                if approval_status:
                    filter_queries['approval_status'] = approval_status
                    filter_queries['vendor__user'] = user

                offer_list = Offer.objects.filter(
                                        **filter_queries).order_by('-updated_on')
        else:
            offer_list = Offer.objects.filter(vendor__user=user)

    
    offer_serializer = OfferListSerializer(
        offer_list[old:new], many=True)
    max_pages = math.ceil(len(offer_list)/per_page)

    return Response({
        'data': offer_serializer.data,
        'code': 200,
        "max_pages": max_pages
    }, status=status.HTTP_200_OK)



def get_employee_list(request):
    '''
    Get Offer list. 10 records per page by default. 
    Filter options: 
        1) Active or inactive
        2) role based
      
    '''
    data = request.query_params
    filter_queries = {}
    
    # Roles 
    SUPER_ADMIN =5
    SALES = 4
    ADMIN = 3

    if not data.get('page'):
        page = 1
    else:
        page = int(data.get('page'))

    if not data.get('per_page'):
        per_page = 10
    else:
        per_page = int(data.get('per_page'))

    if not data.get('role'):
        role = 'all'
    else:
        role = data.get('role')
    user = request.user

    # Check if user has access to the app
    # has_access, response = check_access(user)
    # if not has_access:
    #     return response

    old = (int(page) - 1) * per_page
    new = int(page) * per_page
   

    # filter by active status
    if data.get('is_active'):
        filter_queries['is_active'] = data.get('is_active')

    # filter by role
    if role != 'all':
        filter_queries['role'] = role
        user_list = User.objects.filter(
            **filter_queries).order_by('-name')
    else:
        user_list = User.objects.filter(
        Q(role=ADMIN) | Q(role=SALES) | Q(role=SUPER_ADMIN)).order_by('-name')
    user_serializer = UserSerializer(
        user_list[old:new], many=True)
    max_pages = math.ceil(len(user_list)/per_page)

    return Response({
        'data': user_serializer.data,
        'code': 200,
        "max_pages": max_pages
    }, status=status.HTTP_200_OK)

def toggle_user_inactive(request):
    '''
    Toggle User Inactive or active
    '''
    data = request.data
    user = request.user
    error = False
    error_message = None

    user_id = data.get('user_id',None)
    is_active = data.get('is_active',None)
    print(user_id,is_active)
    if user_id is not None and is_active is not None:
        try :
            employee_instance = User.objects.get(id=user_id)
            if employee_instance.is_active != is_active:
                employee_instance.is_active = is_active
                employee_instance.save()
        except User.DoesNotExist:
            error = True
            error_message = "User not Found"
    else:
        error = True
        error_message = "Fields are required"
    
    if not error:
        return Response({
            'code': 200,
            'message': 'Active Updated'
        }, status=status.HTTP_200_OK)

    else:
        return Response({
            'code': 400,
            'error': error_message,
            'message': "Error in updating Status",
        }, status=status.HTTP_400_BAD_REQUEST)


def get_vendor_list(request):
    '''
    Get Vendor list. 10 records per page by default. 
    Filter options: 
        1) Active or inactive
    '''
    data = request.query_params
    filter_queries = {}
    
    # Roles 
    VENDOR = 1
   

    if not data.get('page'):
        page = 1
    else:
        page = int(data.get('page'))

    if not data.get('per_page'):
        per_page = 10
    else:
        per_page = int(data.get('per_page'))

    user = request.user

    # Check if user has access to the app
    # has_access, response = check_access(user)
    # if not has_access:
    #     return response

    old = (int(page) - 1) * per_page
    new = int(page) * per_page
    search_query = data.get('search')
    role = user.role.name
    print(role)
    if role != 1:
    # filter by active status
        if data.get('search'):
            filter_queries['company_name__icontains'] = data.get('search')
            filter_queries['mobile__icontains'] = data.get('search')
            filter_queries['qr_code__icontains'] = data.get('search')

            vendor_list = Vendor.objects.filter(
            Q(company_name__icontains = search_query) | Q(owner_name__icontains = search_query) | Q(mobile__icontains = search_query) | Q(qr_code__icontains = search_query)).order_by('-updated_on')
        else:
            vendor_list = Vendor.objects.all()

    else:
        vendor_list = Vendor.objects.filter(
            Q(user = user)).order_by('-updated_on')

    vendor_serializer = VendorListSerializer(
        vendor_list[old:new], many=True)
    max_pages = math.ceil(len(vendor_list)/per_page)

    return Response({
        'data': vendor_serializer.data,
        'code': 200,
        "max_pages": max_pages
    }, status=status.HTTP_200_OK)


def vendor_details(request,vendor_id):
    '''
    Vendor Information
    '''
    error = False
    vendor_det = None
    if vendor_id:
        try:
            vendor_info = Vendor.objects.get(id=vendor_id)
            vendor_serializer = VendorDetailSerializer(vendor_info)
            vendor_det = vendor_serializer.data
        except Vendor.DoesNotExist:
            error = True
    else:
        error = True

    if error:
        return Response({
                'message': "Vendor not found",
                'code': 404,
                }, status=status.HTTP_404_NOT_FOUND)
    else: 
        return Response({
                'data': vendor_det,
                'code': 200,
                }, status=status.HTTP_200_OK)

def get_categories_list(request):
    '''
    get categories
    '''
    user = request.user
    data = request.query_params

    search_query = data.get('search',None)

    category_list = get_categories(search_query)
    return Response({
                'data': category_list,
                'code': 200,
                }, status=status.HTTP_200_OK)
        
def check_access(user):
    '''
    Check role and access to the app
    '''
    role = user.role

    if role != 1:
        return False, Response({
            'code': 401,
            'message': 'Unauthorised'
        }, status=status.HTTP_401_UNAUTHORIZED)
    else:
        True, None

def get_categories(search = None):
    '''
    Get all categories or search
    '''
    if search is None:
        category_instance = ProductCategory.objects.all()
    else:
        category_instance = ProductCategory.objects.filter(name__icontains = search).order_by("-name")
    
    category_serializer = ProductCategorySerializer(category_instance,many=True)

    return category_serializer.data

 
def has_permission(role):
    '''
    Check role and access to the app
    '''
    role = user.role

    if role != 1:
        return False, Response({
            'code': 401,
            'message': 'Unauthorised'
        }, status=status.HTTP_401_UNAUTHORIZED)
    else:
        True, None



def send_sms(payload, sms_type):
    '''
    Send sms to respective user based on sms_type
    '''
    conn = http.client.HTTPSConnection("api.msg91.com")

  

    headers = {
    'authkey': "297308AlnxvgeuMg5e1c71fbP1",
    'content-type': "application/json"
    }

    conn.request("POST", "/api/v2/sendsms",json.dumps(payload), headers)

    res = conn.getresponse()
    data = res.read()

    print(data.decode("utf-8"))

def send_email(vendor_name,mobile,password):
    '''
    VEndor Password
    '''
  
    subject = 'Credential for '+vendor_name
    message = "Dear Team, Credential for xxx"+mobile[5:]+" is "+password
    send_mail(subject, 
        message, EMAIL_HOST_USER, ['gncorpcbe@gmail.com'], fail_silently = False)



