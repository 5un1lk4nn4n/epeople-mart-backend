from django.db import models
from django.contrib import admin
from app.accounts.models import User
from app.fileupload.models import UserUpload

# from django.contrib.auth.models import PermissionsMixins
# from django.contrib.auth.base_user import AbstractBaseUser


# list of product category
class ProductCategory(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False)
    icon = models.CharField(max_length=100, blank=False, null=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'product_category'


class Analytics(models.Model):

    count = models.CharField(
        max_length=100, blank=False, null=False, default=0)
    title = models.CharField(
        max_length=100, blank=False, null=False, default="usage")

    class Meta:
        db_table = 'analytics'


# sales team and vendor link
class MarketingSale(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=False)  # sales user id
    vendor = models.ForeignKey(
        'vendors.Vendor', on_delete=models.SET_NULL, null=True, blank=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'marketing_sale'


class SaleEnquiry(models.Model):
    NEW = 'N'
    COMPLETED = 'C'
    STATUS = (
        (NEW, 'New'),
        (COMPLETED, 'Completed'),
    )

    name = models.CharField(max_length=100, blank=False, null=False)
    mobile = models.CharField(max_length=15, blank=False, null=False)
    comments = models.CharField(max_length=1000, blank=True, null=True)
    status = models.CharField(max_length=15, choices=STATUS, default=NEW)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'sale_enquiry'


class Attachment(models.Model):
    AGREEMENT = 'A'
    PROFILE = 'P'
    APPROVAL = 'AP'
    OFFER = 'O'
    LOGO = 'L'
    FILE_TYPE = (
        (AGREEMENT, 'Agreement'),
        (APPROVAL, 'Approval'),
        (OFFER, 'Offer'),
        (LOGO, 'Logo'),
    )

    attachment = models.ForeignKey(
        UserUpload, on_delete=models.SET_NULL, null=True, blank=False)
    attachment_type = models.CharField(
        max_length=15, choices=FILE_TYPE, default=AGREEMENT)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'attachment'


admin.site.register(ProductCategory)
admin.site.register(MarketingSale)
admin.site.register(SaleEnquiry)
admin.site.register(Attachment)
admin.site.register(Analytics)
