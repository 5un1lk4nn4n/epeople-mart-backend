from rest_framework import serializers
from django.utils import timezone

from app.admins.models import ProductCategory, MarketingSale, \
    SaleEnquiry, Attachment

from app.vendors.models import Vendor


class ProductCategorySerializer(serializers.ModelSerializer):
    vendor_count = serializers.SerializerMethodField()

    def get_vendor_count(self, obj):
        vendor_list = Vendor.objects.filter(category=obj.id)
        return len(vendor_list)

    class Meta:
        model = ProductCategory
        fields = ('id', 'name', 'icon', 'vendor_count')


class MarketingSaleSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketingSale
        fields = '__all__'


class SaleEnquirySerializer(serializers.ModelSerializer):

    class Meta:
        model = SaleEnquiry
        fields = '__all__'


class AttachmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Attachment
        fields = ('id', 'url')
