from django.conf.urls import url
from django.urls import path
from app.admins import views as adminView
urlpatterns = [
    # API Url's

    path('create-new-credintial', view=adminView.create_new_credintials),
    path('register-vendor', view=adminView.register_vendor),
    path('offers', view=adminView.offers),
    path('restore-register-vendor',
         view=adminView.restore_vendor_registration),
    path('employees', view=adminView.employees),
    path('vendors', view=adminView.vendors),
    path('vendors/<vendor_id>', view=adminView.vendor_information),
    path('categories', view=adminView.categories),

]
