from django.apps import AppConfig
from django.conf import settings


class adminsConfig(AppConfig):
    name = 'app.admins'
    print("Hello")

    def ready(self):
        print("ready")

        from . import scheduler
        if settings.SCHEDULER_AUTOSTART:
            scheduler.start()
