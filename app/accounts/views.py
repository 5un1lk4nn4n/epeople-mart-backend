import math
from app.admins.models import Attachment, SaleEnquiry, MarketingSale
from app.vendors.models import Vendor, Offer, Location
from app.vendors.serializers import VendorSerializer, OfferSerializer, LocationSerializer
from app.admins.serializers import ProductCategorySerializer, MarketingSaleSerializer, \
    AttachmentSerializer, SaleEnquirySerializer
from datetime import datetime
from django.db import IntegrityError
from app.accounts.models import User, Role
from rest_framework_jwt.settings import api_settings
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from app.accounts.backends import JSONWebTokenAuthentication
import logging
import base64
import hashlib
import uuid
import codecs
import json
import http.client
import random


# logging.b
logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

# Get the JWT settings, add these lines after the import/from lines
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER


@api_view(['POST'])
@permission_classes((AllowAny,))
def login(request):
    """
    Vendor or user or admin login
    """
    if request.method == "POST":
        return access_login(request)


@api_view(['POST'])
@permission_classes((AllowAny,))
def registration(request):
    """
    Premium user Registration only 
    """
    if request.method == "POST":
        return create_premium_user(request)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def me(request):
    """
    my Profile
    """
    if request.method == "POST":
        return my_profile(request)


@api_view(['POST'])
@permission_classes((AllowAny,))
def otp_request(request):
    """
    Forget password step one. Sent OTP to user entered mobile number
    """
    if request.method == "POST":
        return generate_otp(request)


@api_view(['POST'])
@permission_classes((AllowAny,))
def otp_verify(request):
    """
    Forget password step two. verify OTP and Change new password
    """
    if request.method == "POST":
        return verify_otp(request)


@api_view(['PUT'])
@permission_classes((IsAuthenticated,))
def change_password(request):
    '''
    Change password based on user request
    '''
    if request.method == "PUT":
        return update_password(request)


def access_login(request):
    '''
    Check password and allow access to application
    Parameters:
            mobile, password
    '''
    mobile = request.data.get('mobile', None)
    password = request.data.get('password', None)

    token = None
    name = None
    is_first_login = False
    is_change_password_required = False
    last_login = None
    role = None
    error = False
    if mobile and password:
        try:
            user_instance = User.objects.get(mobile=mobile)
            if user_instance.is_active and user_instance.check_password(password):
                token = create_token(user_instance)
                name = user_instance.name
                is_first_login = user_instance.is_first_login
                is_change_password_required = user_instance.is_change_password_required
                last_login = user_instance.last_login_date
                role = user_instance.role.name
                user_instance.current_login_date = datetime.now()
                user_instance.save()
            else:
                error = True

        except User.DoesNotExist:
            error = True
    else:
        error = True

    if error:
        return Response({
            'code': 401,
            'message': "Unauthorised",
        }, status=status.HTTP_401_UNAUTHORIZED)

    else:
        return Response({
            'code': 200,
            'data': {
                'token': token,
                'name': name,
                'image': "http://localhost:3001/static/media/logo.e22af5c4.png",
                'is_first_login': is_first_login,
                'is_change_password_required': is_change_password_required,
                'last_login': last_login,
                'role': role
            }
        }, status=status.HTTP_200_OK)


def create_token(user):
    '''
    create jwt token
    '''
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return token


def update_password(request):
    '''
    update user with new password
    '''
    error = False
    error_message = None
    data = request.data
    user = request.user
    new_password = data.get('new_password', None)
    old_password = data.get('old_password', None)

    if new_password is not None and old_password is not None:
        try:
            if user.check_password(old_password):
                user.set_password(new_password)
                user.otp = None
                user.save()
            else:
                error = True
                error_message = "Invalid Information"

        except User.DoesNotExist:
            error = True
            error_message = "Invalid Information"
    else:
        error = True
        error_message = "Invalid Information"

    if error:
        return Response({
            'code': 400,
            'message': error_message
        }, status=status.HTTP_400_BAD_REQUEST)

    else:
        return Response({
            'code': 200,
            'message': 'Password update successfull'

        }, status=status.HTTP_200_OK)


def generate_otp(request):
    '''
    OTP Request for verification - password change or login
    '''
    error = False
    error_message = None
    data = request.data
    mobile = data.get('mobile', None)
    if mobile is not None:
        try:
            user_instance = User.objects.get(mobile=mobile)
            # generate 4 digit otp
            otp = random_otp()
            user_instance.otp = otp
            user_instance.save()
            payload = {
                "sender": "EPMART",
                "route": "4",
                "country": "91",
                "sms": [
                    {
                        "message": "OTP for epeoplemart access is "+otp,
                        "to": [mobile]
                    }
                ]
            }
            send_sms(payload, 'otp')
        except User.DoesNotExist:
            error = True
            error_message = "User not found"

    if error:
        return Response({
            'code': 400,
            'message': error_message
        }, status=status.HTTP_400_BAD_REQUEST)

    else:
        return Response({
            'code': 200,
            'message': 'OTP has been sent to user mobile'

        }, status=status.HTTP_200_OK)


def random_otp():

    # Declare a digits variable
    # which stores all digits
    digits = "0123456789"
    OTP = ""

   # length of password can be chaged
   # by changing value in range
    for i in range(4):
        OTP += digits[math.floor(random.random() * 10)]

    return OTP


def verify_otp(request):
    '''
    OTP Request for verification - password change or login
    '''
    error = False
    error_message = None
    data = request.data
    mobile = data.get('mobile', None)
    otp = data.get('otp', None)
    new_password = data.get('password', None)

    if mobile is not None and otp is not None and new_password is not None:
        try:
            user_instance = User.objects.get(mobile=mobile)
            if user_instance.otp == otp:
                user_instance.set_password(new_password)
                user_instance.otp = None
                user_instance.save()
            else:
                error = True
                error_message = "Invalid Information"

        except User.DoesNotExist:
            error = True
            error_message = "Invalid Information"
    else:
        error = True
        error_message = "Invalid Information"

    if error:
        return Response({
            'code': 400,
            'message': error_message
        }, status=status.HTTP_400_BAD_REQUEST)

    else:
        return Response({
            'code': 200,
            'message': 'Password change successfull. Please login again.'

        }, status=status.HTTP_200_OK)


def create_premium_user(request):
    '''
    Create premium user
    '''
    error = False
    error_message = None
    token = None

    data = request.data
    name = data.get('name', None)
    mobile = data.get('mobile', None)
    password = data.get('password', None)

    if name is not None and mobile is not None and password is not None:
        role_instance = Role.objects.get(name=2)
        user_instance = User.objects.create_user(name=name,
                                                 mobile=mobile,
                                                 role=role_instance
                                                 )
        user_instance.set_password(password)
        user_instance.save()
        token = create_token(user_instance)
    else:
        error = True
        error_message = "All Fields are required"

    if error:
        return Response({
            'code': 400,
            'message': error_message
        }, status=status.HTTP_400_BAD_REQUEST)

    else:
        return Response({
            'code': 200,
            'token': token,
            'message': 'User Registration Successful'

        }, status=status.HTTP_200_OK)


def send_sms(payload, sms_type):
    '''
    Send sms to respective user based on sms_type
    '''
    conn = http.client.HTTPSConnection("api.msg91.com")

    headers = {
        'authkey': "297308AlnxvgeuMg5e1c71fbP1",
        'content-type': "application/json"
    }

    conn.request("POST", "/api/v2/sendsms", json.dumps(payload), headers)

    res = conn.getresponse()
    data = res.read()

    print(data.decode("utf-8"))


def my_profile(request):
    '''
    Get user profile
    '''
    user = request.user
    return Response({
            'code': 200,
            'data': {
                "name":user.name,
                "mobile":user.mobile,
                "role":user.role.name
            },
            'message': 'User information'

        }, status=status.HTTP_200_OK)