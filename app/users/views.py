from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import status
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings
from app.accounts.models import User, Role
from django.db import IntegrityError
from django.db.models import Q
from django.core.mail import send_mail
from api.settings import EMAIL_HOST_USER

from datetime import datetime, timedelta, date
from app.admins.serializers import ProductCategorySerializer, MarketingSaleSerializer, \
    AttachmentSerializer, SaleEnquirySerializer
from app.admins.views import get_categories
from app.vendors.serializers import VendorSerializer, OfferSerializer, LocationSerializer, \
    OfferUserSerializer, VendorLocationSerializer, OfferListSerializer, OfferSearchSerializer, \
        VendorFreeSerializer, VendorPlaceSerializer, LocationSuggestionSerializer, VendorListSerializer
from app.users.models import SearchHistory, PurchaseHistory, UserPayment
from app.users.serializers import SearchHistorySerializer, PurchaseHistorySerializer, UserPaymentSerializer
from app.vendors.models import Vendor, Offer, Location
from app.admins.models import Attachment, SaleEnquiry, MarketingSale, ProductCategory, Analytics
import http.client
import re
import json
import math
import random
from spellchecker import SpellChecker
from django.db import connections

from geopy import units, distance

@api_view(['GET'])
@permission_classes((AllowAny,))
def home(request):
    return get_home_features(request)


@api_view(['POST'])
@permission_classes((AllowAny,))
def around_me(request):
    if request.method == "POST":
        return find_around_me(request)


@api_view(['POST'])
@permission_classes((AllowAny,))
def search(request):
    return search_or_list_offer(request)


@api_view(['POST'])
@permission_classes((AllowAny,))
def search_suggestion(request):
    return get_suggestion(request)


@api_view(['POST'])
@permission_classes((AllowAny,))
def track(request):
    return track_queries(request)


@api_view(['POST'])
@permission_classes((AllowAny,))
def vendor_view_count(request):
    return increment_view_count(request)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def authorise_sale(request):
    return authorise_user_sale(request)


@api_view(['POST'])
@permission_classes((AllowAny,))
def enquiry(request):
    return create_new_enquiry(request)




@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def finalise_sale(request):
    return finalise_user_sale(request)


@api_view(['GET'])
@permission_classes((AllowAny,))
def categories(request):
    if request.method == "GET":
        return get_categories_list(request)


@api_view(['GET'])
@permission_classes((AllowAny,))
def vendors(request):
    if request.method == "GET":
        return get_vendor_list(request)

@api_view(['GET'])
@permission_classes((AllowAny,))
def usage_count(request):
    if request.method == "GET":
        analytic_instance = Analytics.objects.get(title='usage')
        return Response({
        'code': 200,
        'usage':analytic_instance.count,
        'message': 'Usage Views Ok'
    }, status=status.HTTP_200_OK)
        


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def transactions(request):
    return get_transaction_history(request)


def get_home_features(request):
    '''
    Get Home features. Random 8 offers, Random 3 queries
    '''
    user = request.user
    filter_queries = {}
    offers = []
    today_offers = []

    # statistic usage count 
    increment_usage_count()
    filter_queries['is_active'] = True
    today = datetime.today()
    offer_instances = Offer.objects.filter(**filter_queries)

    offers_for_today = Offer.objects.filter(
        from_date__year=today.year, from_date__month=today.month, from_date__day=today.day, to_date__year=today.year, to_date__month=today.month, to_date__day=today.day,is_active = True)

    # today Offer serialiser

    today_offer_serializer = OfferUserSerializer(
        offers_for_today, many=True)
    today_offers = today_offer_serializer.data
    random.shuffle(today_offers)
    today_offers = today_offers[:8]
    if len(offer_instances) > 8:
        offer_serializer = OfferUserSerializer(
            offer_instances, many=True)
        offers = offer_serializer.data
        random.shuffle(offers)
        offers = offers[:8]

    else:
        offer_serializer = OfferUserSerializer(
            offer_instances, many=True)
        offers = offer_serializer.data
        random.shuffle(offers)

    return Response({
        'data': {"offers": offers, "today": today_offers},
        'code': 200,
    }, status=status.HTTP_200_OK)

def increment_usage_count():
    '''
    Increment application usage count
    '''
    analytic_instance = Analytics.objects.get(title='usage')
    current_count = int(analytic_instance.count)
    analytic_instance.count = current_count + 1
    analytic_instance.save()



def increment_view_count(request):
    '''
    Increment Vendor View Count
    '''

    data = request.data
    vendor_id = data.get('vendor_id', None)
    if vendor_id:
        try:
            vendor_instance = Vendor.objects.get(id=vendor_id)
            vendor_instance.views = int(vendor_instance.views) + 1
            vendor_instance.save()
        except:
            pass

    return Response({
        'code': 200,
        'message': 'Vendor Views ok'
    }, status=status.HTTP_200_OK)

def create_new_enquiry(request):
    '''
    Create new enquiry or contact us form
    '''
    data = request.data
    name = data.get('name',"")
    email = data.get('email',"")
    mobile = data.get('mobile',"")
    company = data.get('company',"")
    message = data.get('message',"")


    subject = 'Enquiry from Epeoplemart Website'
    message = 'Dear Team,\n'+'User requested enquiry with us. Below are the information.\n'+'Name:'+name+'\nMobile:'+mobile+'\nEmail:'+email+'\nCompany:'+company+'\nMessage:'+message
    send_mail(subject, 
        message, EMAIL_HOST_USER, ['gncorpcbe@gmail.com','support@epeoplemart.com'], fail_silently = False)
    
    return Response({
        'code': 200,
        'message': 'Thank you for contacting with us! our executive will call you back.'
    }, status=status.HTTP_200_OK)



def get_categories_list(request):
    '''
    get categories
    '''
    user = request.user
    data = request.query_params

    search_query = data.get('search', None)
    if not data.get('page'):
        page = 1
    else:
        page = int(data.get('page'))

    if not data.get('per_page'):
        per_page = 15
    else:
        per_page = int(data.get('per_page'))
    
    old = (int(page) - 1) * per_page
    new = int(page) * per_page

    if search_query is None:
        category_instance = ProductCategory.objects.all()
    else:
        category_instance = ProductCategory.objects.filter(name__startswith = search_query).order_by("-name")
    
    category_serializer = ProductCategorySerializer(category_instance[old:new],many=True)
    max_pages = math.ceil(len(category_instance)/per_page)

    return Response({
        'data': category_serializer.data,
        'code': 200,
        "max_pages": max_pages
        }, status=status.HTTP_200_OK)



def get_suggestion(request):
    '''
    Get Suggestion for search query
    '''

    user = request.user
    data = request.data

    # Search Type
    CATEGORY = 1
    QUERY = 2

    search_query = data.get('search', None)
    location_query = data.get('location', None)
    suggestion = {

    }


    print(data)
    if search_query:
        offer_list = Offer.objects.filter(
            Q(vendor__company_name__icontains=search_query) | Q(tag__icontains=search_query) | Q(title__icontains=search_query) | Q(vendor__category__name__icontains=search_query) , Q(is_active=True)).order_by("-discount")
        offer_serializer = OfferSearchSerializer(
            offer_list[:2], many=True)
        vendor_list = Vendor.objects.filter(
            Q(company_name__icontains=search_query) & Q(user__is_active=True))
        vendor_serializer = VendorSerializer(
            vendor_list[:5], many=True)

        categories = get_categories(search_query)
        

        suggestion = {
            "offer": offer_serializer.data,
            "vendor": vendor_serializer.data,
            "categories": categories[:5]
        }
    elif location_query:
        location_list = Location.objects.filter(Q(address_line_2__startswith=location_query)).distinct('address_line_2')
        location_list_serializer = LocationSuggestionSerializer(location_list[:15],many=True)
        suggestion={
            'location':location_list_serializer.data
        }
    else:
        categories = []
        one_week_ago = datetime.today() - timedelta(days=7)

        top_search_queries_by_query = SearchHistory.objects.filter(
            created_on__gte=one_week_ago, query_type=CATEGORY).order_by('-score')

        top_search_queries_by_categories = SearchHistory.objects.filter(
            created_on__gte=one_week_ago, query_type=QUERY
        ).order_by('-score')

        search_history_query_serilaizer = SearchHistorySerializer(
            top_search_queries_by_query[:5], many=True)

        search_history_categories_serilaizer = SearchHistorySerializer(
            top_search_queries_by_categories[:5], many=True)
        if len(search_history_categories_serilaizer.data) == 0 :
            categories = get_categories(search_query)
            random.shuffle(categories)
            categories = categories[:3]
        else:
            categories = search_history_categories_serilaizer.data
        
        places = Vendor.objects.filter(user__is_active = True )
        
        place_serializer = VendorPlaceSerializer(places,many=True)


        suggestion = {
            "top_query": search_history_query_serilaizer.data,
            "top_categories": categories,
            "top_location": place_serializer.data
        }

    return Response({
        'data': suggestion,
        'code': 200,
    }, status=status.HTTP_200_OK)


def authorise_user_sale(request):
    '''
    After scanning QR code, user will be athenticated to claim offer in shop
    '''

    user = request.user
    data = request.data
    is_ok = False
    vendor_shop_name = None
    vendor_id = None
    qr_code = data.get('code', None)

    if qr_code:
        try:
            vendor_instance = Vendor.objects.get(qr_code=qr_code)
            is_ok = True
            vendor_shop_name = vendor_instance.company_name
            vendor_id = vendor_instance.id
        except Vendor.DoesNotExist:
            pass

    return Response({
        'data': {
            'is_authenticated': is_ok,
            'shop': vendor_shop_name,
            'vendor_id': vendor_id
        },
        'code': 200,
    }, status=status.HTTP_200_OK)


def finalise_user_sale(request):
    '''
    After Qr code authorisation, User enters total bill value and close offer.
    '''
    user = request.user
    data = request.data

    amount = data.get('amount')
    vendor_id = data.get('vendor_id')

    if amount and vendor_id:
        new_purchase_history = {
            "vendor": vendor_id,
            "user": user,
            "amount": amount
        }

        purchase_history_serializer = PurchaseHistorySerializer(
            data=new_purchase_history)

        if purchase_history_serializer.is_valid():
            purchase_history_serializer.save()

    return Response({
        'message': "Thank you for shopping with us!",
        'code': 200,
    }, status=status.HTTP_200_OK)


def get_transaction_history(request):
    '''
    Show user purchase history
    '''
    filter_queries = {}
    data = request.query_params
    if not data.get('page'):
        page = 1
    else:
        page = int(data.get('page'))

    if not data.get('per_page'):
        per_page = 10
    else:
        per_page = int(data.get('per_page'))

    user = request.user

    old = (int(page) - 1) * per_page
    new = int(page) * per_page

    filter_queries['user'] = user

    purchase_history = PurchaseHistory.objects.filter(
        **filter_queries).order_by('-created_on')
    purchase_history_serializer = PurchaseHistorySerializer(
        purchase_history[old:new], many=True)
    max_pages = math.ceil(len(purchase_history)/per_page)

    return Response({
        'data': purchase_history_serializer.data,
        'code': 200,
        "max_pages": max_pages
    }, status=status.HTTP_200_OK)


def find_around_me(request):
    '''
    Get near by shops from my current location
    '''
    user = request.user
    data = request.data
    latitude = data.get('latitude', None)
    longitude = data.get('longitude', None)
    near_me_vendors = []
    if not data.get('page'):
        page = 1
    else:
        page = int(data.get('page'))

    if not data.get('per_page'):
        per_page = 15
    else:
        per_page = int(data.get('per_page'))
    old = (int(page) - 1) * per_page
    new = int(page) * per_page
    max_pages = 0

    # distance radius. By default 1 km radius
    distance_range = 1

    if latitude and longitude:
        latitude       = float(latitude)
        longitude      = float(longitude)
        distance_range = float(distance_range)   
        rough_distance = units.degrees(arcminutes=units.nautical(kilometers=distance_range)) * 2

        near_by_locations = Location.objects.filter(
            latitude__range=(
                latitude - rough_distance,
                latitude + rough_distance
            ),
            longitude__range=(
                longitude - rough_distance,
                longitude + rough_distance
            )
        )
        locations = []
        for location in near_by_locations:
          if location.latitude and location.longitude:
            exact_distance = distance.distance(
                (latitude, longitude),
                (location.latitude, location.longitude)
            ).kilometers

            if exact_distance <= distance_range:
              locations.append(location)

        vendor_instances = Vendor.objects.filter(location__in=[l.id for l in locations],user__is_active = True)
        max_pages = math.ceil(len(vendor_instances)/per_page)
      
        vendor_list_serializer = VendorLocationSerializer(
        vendor_instances[old:new], many=True)


        return Response({
            'data': vendor_list_serializer.data,
            'code': 200,
            "max_pages":max_pages
        }, status=status.HTTP_200_OK)

    else:
         return Response({
        'code': 400,
        "message":"Invalid Request"
    }, status=status.HTTP_400_BAD_REQUEST)

def raw_query(sql, replacements, many):
    """
    Raw Query service using cursor with SQLquery, replacements and multiple rows flag as parameters
    """
    with connections['default'].cursor() as cursor:
        cursor.execute(sql, replacements)
        if many:
            columns = [x[0] for x in cursor.description]
            return [
                dict(zip(columns, row))
                for row in cursor.fetchall()
            ]
        columns = (x[0] for x in cursor.description)
        query_data = cursor.fetchone()
        return dict(zip(columns, query_data))


def search_or_list_offer(request):
    '''
    search offers by category, vendor, search string and near me location. Other special searches such as one day offers and week end offers
    combinations:
    search query & category & [is_today | is week end] & vendor
    search query & category & [is_today | is week end] & vendor
    '''
    user = request.user
    data = request.data
    print(data,"sear")

    search_query = data.get('search', None)
    category = data.get('category', None)
    is_today = data.get('is_today', None)
    # is_week_end = data.get('is_week_end', None)
    vendor = data.get('vendor_id', None)
    location = data.get('location', None)

    if not data.get('page'):
        page = 1
    else:
        page = int(data.get('page'))

    if not data.get('per_page'):
        per_page = 10
    else:
        per_page = int(data.get('per_page'))

    old = (int(page) - 1) * per_page
    new = int(page) * per_page
    offer_list=[]
    offer_list_by_search =[]
    offer_list_by_category =[]
    offer_list_by_vendor =[]
    offer_list_by_today =[]
    print(date.today())
    if search_query:
        offer_list_by_search = Offer.objects.filter(
        Q(vendor__company_name__icontains = search_query) | Q(tag__icontains = search_query) |Q(title__icontains = search_query), Q(is_active =True))

        # if category:
        #     offer_list_by_category = Offer.objects.filter(vendor__category = category,is_active=True)
        #     offer_list_by_search = getCommonRecords(offer_list_by_search,offer_list_by_category)
        if vendor:
            offer_list_by_vendor = Offer.objects.filter(vendor=vendor,is_active=True)
            offer_list_by_search = getCommonRecords(offer_list_by_search,offer_list_by_vendor)

        if location:
            offer_list_by_location = Offer.objects.filter(Q(vendor__location__address_line_2__icontains = location), Q(is_active = True))
            offer_list_by_search = getCommonRecords(offer_list_by_search,offer_list_by_location)

        if is_today:
            offer_list_by_today = Offer.objects.filter(from_date=date.today(), to_date = date.today(),is_active=True)
            offer_list_by_search = getCommonRecords(offer_list_by_search,offer_list_by_today)

        
        # offer_list = [*offer_list_by_today,*offer_list_by_category,*offer_list_by_vendor,*offer_list_by_search]
        offer_list = offer_list_by_search
    if category and location:
        offer_list = Offer.objects.filter(Q(vendor__location__address_line_2__icontains = location) & Q(vendor__category__name__icontains = category), Q(is_active = True) )

    if category and not location:
        offer_list = Offer.objects.filter(vendor__category__name__icontains =category,is_active=True)
    if vendor:
        offer_list = Offer.objects.filter(vendor=vendor,is_active=True)
    if location and not category:
        print(location,"wo")
        offer_list = Offer.objects.filter(vendor__location__address_line_2__icontains =location,is_active=True)
        print(offer_list)
    if is_today:
        offer_list = Offer.objects.filter(from_date=datetime.today(), to_date = datetime.today(),is_active=True)
    if not search_query and not category and not vendor  and not is_today and not location :
        offer_list = Offer.objects.filter(is_active=True)


    
    offer_serializer = OfferListSerializer(
        offer_list[old:new], many=True)
    max_pages = math.ceil(len(offer_list)/per_page)

    return Response({
        'data': offer_serializer.data,
        'code': 200,
        "max_pages": max_pages
    }, status=status.HTTP_200_OK)

def getCommonRecords(lst1, lst2): 
    return list(set(lst1) & set(lst2)) 


def get_vendor_list(request):
    '''
    Get Vendor list. 10 records per page by default. 
    Filter options: 
        1) Active or inactive
    '''
    data = request.query_params

    if not data.get('page'):
        page = 1
    else:
        page = int(data.get('page'))

    if not data.get('per_page'):
        per_page = 10
    else:
        per_page = int(data.get('per_page'))
 

    old = (int(page) - 1) * per_page
    new = int(page) * per_page
    search_query = data.get('search',None)
    category_query = data.get('category',None)


    is_mobile = data.get('is_mobile',False)
    # filter by active status
    if is_mobile:
        vendor_list = Vendor.objects.filter(
            Q(company_name__startswith = search_query) | Q(category__name__startswith = search_query))

    else:
        if search_query:
            vendor_list = Vendor.objects.filter(
            Q(company_name__icontains = search_query))
        elif category_query:
            vendor_list = Vendor.objects.filter(
            Q(category__name__icontains = category_query))
        else:
            vendor_list = Vendor.objects.all()

    vendor_serializer = VendorFreeSerializer(
        vendor_list[old:new], many=True)
    max_pages = math.ceil(len(vendor_list)/per_page)

    return Response({
        'data': vendor_serializer.data,
        'code': 200,
        "max_pages": max_pages
    }, status=status.HTTP_200_OK)
