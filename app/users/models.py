from django.db import models
from django.contrib import admin
from app.accounts.models import User
from app.vendors.models import Vendor

# from django.contrib.auth.models import PermissionsMixins
# from django.contrib.auth.base_user import AbstractBaseUser


class UserPayment(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=False)
    amount = models.DecimalField(max_digits=10, decimal_places=2)

    payment_date = models.DateTimeField(auto_now_add=True)
    transaction_id = models.CharField(max_length=150, blank=False, null=False)
    bank = models.CharField(max_length=50, blank=True, null=True)

    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'user_payment'


class PurchaseHistory(models.Model):

    vendor = models.ForeignKey(
        Vendor, on_delete=models.SET_NULL, null=True, blank=False)
    user = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=False)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'purchase_history'


class SearchHistory(models.Model):
    CATEGORY = 1
    QUERY = 2

    QUERY_CHOICES = (
        (CATEGORY, 'category'),
        (2, 'query'),

    )

    query = models.CharField(max_length=200, blank=False, null=False)
    query_type = models.CharField(
        max_length=15, choices=QUERY_CHOICES, default=QUERY)
    score = models.CharField(
        max_length=200, blank=False, null=False, default=0)

    created_on = models.DateField(auto_now_add=True)
    updated_on = models.DateField(auto_now=True)

    class Meta:
        db_table = 'search_history'


admin.site.register(UserPayment)
admin.site.register(PurchaseHistory)
admin.site.register(SearchHistory)
