from django.conf.urls import url
from django.urls import path
from app.users import views as userView

urlpatterns = [
    # API Url's

    path('home', view=userView.home),
    path('around-me', view=userView.around_me),
    path('search', view=userView.search),
    path('enquiry', view=userView.enquiry),
    path('suggestion', view=userView.search_suggestion),
    path('authosire-sale', view=userView.authorise_sale),
    path('finalise', view=userView.finalise_sale),
    path('categories', view=userView.categories),
    path('vendors', view=userView.vendors),
    path('track', view=userView.track),
    path('transactions', view=userView.transactions),
    path('vendor/views/',
         view=userView.vendor_view_count),
    path('track/usage/',
         view=userView.usage_count),
]
