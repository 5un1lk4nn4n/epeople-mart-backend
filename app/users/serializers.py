from rest_framework import serializers
from django.utils import timezone

from app.users.models import UserPayment, PurchaseHistory, SearchHistory


class UserPaymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserPayment
        fields = '__all__'


class PurchaseHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = PurchaseHistory
        fields = '__all__'


class SearchHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SearchHistory
        fields = ('query')
