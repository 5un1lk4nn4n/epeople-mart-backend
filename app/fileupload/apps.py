from django.apps import AppConfig


class FileuploadConfig(AppConfig):
    name = 'app.fileupload'
