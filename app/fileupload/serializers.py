from rest_framework import serializers

from app.fileupload.models import UserUpload


class UserUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserUpload
        fields = "__all__"
