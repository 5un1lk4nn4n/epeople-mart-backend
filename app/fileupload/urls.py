from django.conf.urls import url
from django.urls import path
from app.fileupload import views as fileView
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required


urlpatterns = [
    # API Url's

    path('upload', view=fileView.user_file_upload),
    path('upload/vendor', view=fileView.vendor_upload),
    # path('/assets/<file_type>/<file_name>', view=fileView.user_file_upload),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
