from django.db import models
import uuid
import os
from django.contrib import admin


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join(instance.file_type, filename)


class UserUpload(models.Model):
    AGREEMENT = 'A'
    PROFILE = 'P'
    APPROVAL = 'AP'
    OFFER = 'O'
    LOGO = 'L'
    FILE_TYPE = (
        (AGREEMENT, 'Agreement'),
        (PROFILE, 'Profile'),
        (APPROVAL, 'Approval'),
        (OFFER, 'Offer'),
        (LOGO, 'Logo'),
    )
    file = models.FileField(upload_to=get_file_path, blank=False, null=False)
    file_type = models.CharField(
        max_length=15, choices=FILE_TYPE, default=PROFILE)

    def __str__(self):
        return self.file.name


admin.site.register(UserUpload)
