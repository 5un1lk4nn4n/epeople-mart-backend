from rest_framework.parsers import FileUploadParser
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import status
from rest_framework.response import Response
from django.http import FileResponse
from django.core.files.storage import FileSystemStorage
from django.core.files.storage import default_storage
import xlrd
from django.conf import settings
from django.http import JsonResponse
from app.admins.models import ProductCategory
from app.vendors.models import Vendor, Location
from app.vendors.serializers import VendorSerializer, LocationSerializer
from app.accounts.models import User, Role
from app.admins.serializers import ProductCategorySerializer
from app.fileupload.serializers import UserUploadSerializer
from app.fileupload.models import UserUpload
from datetime import datetime


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def user_file_upload(request):
    '''
    User file upload
    '''
    parser_class = (FileUploadParser,)
    file_serializer = UserUploadSerializer(data=request.data)
    if file_serializer.is_valid():
        file_serializer.save()
        response = {
            'code': 201,
            'message': 'File uploaded',
            'data': file_serializer.data
        }
        return Response(response, status=status.HTTP_201_CREATED)
    else:
        response = {
            'code': 400,
            'message': 'Error in Uploading',
            'error': file_serializer.errors
        }
        return Response(response, status=status.HTTP_400_BAD_REQUEST)


@permission_classes((IsAuthenticated,))
def vendor_upload(request):
    '''
    Upload vendor information
    '''

    myfile = request.FILES['excel_file']
    file_name = default_storage.save(myfile.name, myfile)
    errors = []

    all_errors = []
    instance_error = None
    BUSINESS_TYPE = {
        "SMALL": "SM",
        "LARGE": 'LR',
        "MEDIUM": 'MD'
    }

    category_id = None

    error_flag = False
    print(settings.MEDIA_ROOT+'/'+file_name)
    vendor_information = xlrd.open_workbook(
        settings.MEDIA_ROOT+'/'+file_name, encoding_override='utf8')
    work_sheets = vendor_information.sheet_names()
    template_wrk = vendor_information.sheet_by_name(work_sheets[0])
    num_cols = template_wrk.ncols
    num_rows = template_wrk.nrows
    print(num_cols, num_rows)
    for row_idx in range(1, template_wrk.nrows):
        errors = []
        error_flag = False
        owner_name = template_wrk.cell(row_idx, 0).value
        owner_mobile = template_wrk.cell(row_idx, 1).value
        address_line_1 = template_wrk.cell(row_idx, 2).value
        address_line_2 = template_wrk.cell(row_idx, 3).value
        city = template_wrk.cell(row_idx, 4).value
        district = template_wrk.cell(row_idx, 5).value
        state = template_wrk.cell(row_idx, 6).value
        lattitude = template_wrk.cell(row_idx, 7).value
        longitude = template_wrk.cell(row_idx, 8).value
        pin_code = template_wrk.cell(row_idx, 9).value
        company_phone = template_wrk.cell(row_idx, 10).value
        email = template_wrk.cell(row_idx, 11).value
        website = template_wrk.cell(row_idx, 12).value
        gst = template_wrk.cell(row_idx, 13).value
        category = template_wrk.cell(row_idx, 14).value
        business_type = template_wrk.cell(row_idx, 15).value
        discount_percent = template_wrk.cell(row_idx, 16).value
        discount_coupon = template_wrk.cell(row_idx, 17).value
        discount_count = template_wrk.cell(row_idx, 18).value
        coupon_description = template_wrk.cell(row_idx, 19).value
        employee_code = template_wrk.cell(row_idx, 20).value
        company_name = template_wrk.cell(row_idx, 21).value

        category = category.capitalize()
        owner_mobile = str(int(owner_mobile))

        if not owner_name:
            errors.append("Owner Nameis required")
            error_flag = True

        if not owner_mobile:
            errors.append("Owner mobile required")
            error_flag = True
        if owner_mobile:
            try:
                user_instance = User.objects.get(mobile=owner_mobile)
                errors.append("Owner mobile is already registered with us")
                error_flag = True
            except:
                pass

        if not address_line_1 or not address_line_2 or not district or not state or not lattitude or not longitude or not pin_code:
            errors.append("complete Address with location is required")
            error_flag = True
        if lattitude and longitude:
            num_results = Location.objects.filter(
                latitude=lattitude, longitude=longitude).count()
            if num_results > 0:
                errors.append("latidute longitude already exists")
                error_flag = True
        if business_type not in BUSINESS_TYPE.keys():
            errors.append("Busines type should in LARGE, SMALL, MEDIUM")
            error_flag = True

        if category:
            try:
                category_id = ProductCategory.objects.get(name=category)
            except ProductCategory.DoesNotExist:
                new_category = {
                    "name": category,
                    "icon": 'grid'
                }
                category_serialiser = ProductCategorySerializer(
                    data=new_category)
                if category_serialiser.is_valid():
                    category_id = category_serialiser.save()

                else:
                    errors.append("error in addding catgeory")
                    error_flag = True
            except:
                pass
        if not error_flag:
            # create location
            new_location = {
                "latitude": lattitude,
                "longitude": longitude,
                "address_line_1": address_line_1,
                "address_line_2": address_line_2,
                "district": district,
                "state": state,
                "pin_code": pin_code
            }
            location_serializer = LocationSerializer(data=new_location)
            if location_serializer.is_valid():
                loc = location_serializer.save()
            else:
                error_flag = True,
                instance_error = location_serializer.errors
            if not error_flag:
                password = User.objects.make_random_password()
                role_instance = Role.objects.get(name=1)
                user = User.objects.create_user(name=owner_name,
                                                mobile=owner_mobile,
                                                role=role_instance
                                                )
                user.set_password(password)
                user.save()
                if discount_coupon:
                    has_coupon = True
                else:
                    has_coupon = False

                new_vendor = {
                    "company_name": company_name,
                    "location": loc.id,
                    "user": user.id,
                    "owner_name": owner_name,
                    "mobile": owner_mobile,
                    "company_phone": company_phone,
                    "category": category_id.id,
                    "website": website,
                    "email": email,
                    "gst": gst,
                    "business_type": BUSINESS_TYPE[business_type],
                    "logo": None,
                    "discount_percentage": discount_percent,
                    "has_coupon": has_coupon,
                    "coupon_value": discount_coupon,
                    "coupon_count": discount_count,
                    "qr_code": generate_qr_code(company_name),
                    "is_active": True,
                    "aggrement": None,
                    "coupon_description": coupon_description,
                    "sales_by": None,
                    "sales_by_code": employee_code
                }

                vendor_serializer = VendorSerializer(data=new_vendor)

                if vendor_serializer.is_valid():
                    vendor_instances = vendor_serializer.save()
                else:
                    instance_error = vendor_serializer.errors
                    error_flag = True
                    try:
                        Location.objects.get(id=loc.id).delete()
                        User.objects.get(mobile=owner_mobile).delete()
                    except:
                        pass
        if error_flag:
            full_error = {
                "errors": errors,
                "db_error": instance_error,
                "row_no": row_idx
            }
            all_errors.append(full_error)

    return JsonResponse({
        'code': 200,
        'number of records not saved': len(all_errors),
        "error_message": all_errors,
        'message': 'File uploaded',
    }, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def serve_files(request, file_type, file_name):
    '''
    NOTE: PENDING. NEED TO MAKE SURE ONLY ACCESABLE VIA SESSION
    '''
    try:
        file_instance = UserUpload.objects.get(
            file='assets/'+file_name+'/'+file_name)
        response = FileResponse(file_instance.file,)
        response["Content-Disposition"] = "attachment; filename=" + file_name

    except UserUpload.DoesNotExist:
        return Response({"message": "unauthorised"}, status=status.HTTP_400_BAD_REQUEST)


def generate_qr_code(company_name):
    '''
    generate QR code in format EPM<first 3 char of company name><Joined date time>
    '''

    timestamp = datetime.now().strftime('%Y%m%d%H%M%S')
    code = "EPM"+company_name[:3].upper()+timestamp
    return code
